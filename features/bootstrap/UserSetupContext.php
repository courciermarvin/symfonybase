<?php

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Doctrine\ORM\EntityManagerInterface;
use App\Service\UserService;
use App\Entity\User;

class UserSetupContext implements Context
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoderInterface;

    /**
     * UserSetupContext constructor.
     *
     * @param EntityManagerInterface $em
     * @param UserPasswordEncoderInterface $encoderInterface
     */
    public function __construct(
        EntityManagerInterface $em,
        UserPasswordEncoderInterface $encoderInterface
    ) {
        $this->em = $em;
        $this->encoderInterface = $encoderInterface;
    }

    /**
     * Test User Registration
     *
     * @param TableNode $users
     *
     * @Given there are Users with the following details:
     */
    public function thereAreUsersWithTheFollowingDetails(TableNode $users)
    {
        foreach ($users->getColumnsHash() as $key => $val) {

            $confirmationToken = isset($val['confirmation_token']) && $val['confirmation_token'] != ''
                ? $val['confirmation_token']
                : null;

            $user = new User();
            $user->setEnabled(true);
            $user->setFirstname($val['firstname']);
            $user->setEmail($val['email']);
            $user->setPlainPassword($val['password']);
            $password = $this->encoderInterface->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $user->setConfirmationToken($confirmationToken);

            $this->em->persist($user);
            $this->em->flush();
        }
    }

    /**
     * @param TableNode $users
     *
     * @Given there are users Registering
     */
    public function usersRegister(TableNode $users)
    {
        foreach ($users->getColumnsHash() as $key => $value) {
            try {

                $this->iSendARequest('POST', 'register', [
                    'json' => [
                        'email' => $value['email'],
                        'password' => $value['password'],
                    ]
                ]);


                $responseBody = json_decode($this->response->getBody(), true);
                $this->addHeader('Authorization', 'Bearer ' . $responseBody['token']);

            } catch (\GuzzleHttp\Exception\RequestException $e) {

                echo Psr7\str($e->getRequest());

                if ($e->hasResponse()) {
                    echo Psr7\str($e->getResponse());
                }

            }
        }
    }
}
