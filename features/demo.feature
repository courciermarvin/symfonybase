
Feature:
  I want to be able to register a new account

  Scenario:
    Given there are Users with the following details:
      | id | firstname | email          | password |
      | 1  | peter     | peter@test.com | testpass |
      | 2  | john      | john@test.org  | johnpass |
      | 3  | tim       | tim@blah.net   | timpass  |
