<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use App\Serializer\FormErrorSerializer;
use Psr\Log\LoggerInterface;

/**
 * Class DefaultController
 *
 * @category  App
 * @package   App\Controller
 * @author    Marvin Courcier <courciermarvin@gmail.com>
 * @copyright 2019
 */
class DefaultController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var FormErrorSerializer
     */
    protected $formErrorSerializer;

    /**
     * DefaultController Constructor.
     *
     * @param EntityManagerInterface $entityManagerInterface
     * @param LoggerInterface $loggerInterface
     * @param SerializerInterface $serializer
     * @param FormErrorSerializer $formErrorSerializer
     */
    public function __construct(
        EntityManagerInterface $entityManagerInterface,
        LoggerInterface $loggerInterface,
        SerializerInterface $serializer,
        FormErrorSerializer $formErrorSerializer
    ) {
        $this->em = $entityManagerInterface;
        $this->logger = $loggerInterface;
        $this->serializer = $serializer;
        $this->formErrorSerializer = $formErrorSerializer;
    }

    /**
     * Send API Response
     *
     * @param $responseData
     *
     * @return Response
     */
    public function sendResponse($responseData): Response
    {
        $response = new Response(((json_encode($responseData))));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

    /**
     * Persist and Flush Datas
     *
     * @param mixed $data
     * 
     * @return void
     */
    public function flushDatas($data): void
    {
        $this->em->persist($data);
        $this->em->flush();
    }

    /**
     * Tool to print
     *
     * @param mixed $data
     * 
     * @return void
     */
    public function print($data): void
    {
        echo '<pre>';
        print_r($data);
        die();
    }
}
