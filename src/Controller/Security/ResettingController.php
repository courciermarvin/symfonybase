<?php

namespace App\Controller\Security;

use App\Form\UserType;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Controller\DefaultController;
use App\Service\SwiftMailerService;
use App\Entity\User;

/**
 * Class ResettingController
 *
 * @category  App
 * @package   App\Controller
 * @author    Marvin Courcier <courciermarvin@gmail.com>
 * @copyright 2019
 */
class ResettingController extends DefaultController
{
    /**
     * Send Reset Email Password
     *
     * @param Request           $request
     * @param SwiftMailerService $mailer
     * @param UserService        $userService
     *
     * @return JsonResponse
     *
     * @Route("/reset/password", name="app_resetting_resetpassword", methods={"POST"})
     */
    public function sendEmail(Request $request, SwiftMailerService $mailer, UserService $userService): JsonResponse
    {
        $jsonData = json_decode(
            $request->getContent(),
            true
        );

        $userEmail = isset($jsonData['email']) ? $jsonData['email'] : null;
        $user = $userService->findByEmail($userEmail);

        if (!$user || $user->getConfirmationToken() === null) {
            return new JsonResponse(
                [
                    'errors' => 'This ressource does not exist'
                ],
                JsonResponse::HTTP_NOT_FOUND
            );
        }

        $user->setPassword(null);
        $user->generateToken();
        $mailer->resetPassword($user);
        $this->flushDatas($user);

        return new JsonResponse(
            [
                'message' => 'Check your email to reset your password'
            ],
            JsonResponse::HTTP_OK
        );
    }

    /**
     * Resetting password
     *
     * @param Request $request
     * @param string $token
     *
     * @return Response
     *
     * @Route("/reset/password/{token}", name="app_resetting_resetpassword", methods={"GET, POST"})
     */
    public function resetPassword(Request $request, string $token): Response
    {
        $user = $this->em->getRepository(User::class)->getByConfirmationToken($token);

        if (!$user) {
            throw new NotFoundHttpException();
        }

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setConfirmationToken(null);
            $this->flushDatas($user);

            $this->redirectToRoute('app_resetting_resetpasswordsuccess');
        }

        return $this->render('resetting/resetPassword.html.twig', ['form' => $form->createView()]);
    }

    /**
     * Reset Password Success
     *
     * @Route("/reset/password/success", name="app_resetting_resetpasswordsuccess", methods={"GET"})
     */
    public function resetPasswordSuccess(): Response
    {
        return $this->render('resetting/resetPasswordSuccess.html.twig');
    }
}