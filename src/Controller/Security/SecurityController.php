<?php

namespace App\Controller\Security;

use App\Service\UserService;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Controller\DefaultController;
use App\Service\SwiftMailerService;
use App\Form\UserType;
use App\Entity\User;

/**
 * Class SecurityController
 *
 * @category  App
 * @package   App\Controller
 * @author    Marvin Courcier <courciermarvin@gmail.com>
 * @copyright 2019
 */
class SecurityController extends DefaultController
{
    /**
     * Register
     *
     * @param Request     $request
     * @param UserService $userService
     *
     * @return JsonResponse
     *
     * @Route("api/register", name="app_security_apiregister", methods={"POST"})
     */
    public function apiRegister(
        Request $request,
        UserService $userService
    ): JsonResponse {
        $userInformation = json_decode(
            $request->getContent(),
            true
        );

        $newUser = new User;
        $form = $this->createForm(UserType::class, $newUser, ['api' => true]);
        $form->submit($userInformation);

        if (false === $form->isValid()) {
            return new JsonResponse(
                [
                    'errors' => $this->formErrorSerializer->convertFormToArray($form),
                ],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        $user = $userService->findByEmail($newUser->getEmail());

        if (!$user) {
            $userService->registerUser($newUser);
        } else {
            return new JsonResponse(
                [
                    'errors' => 'Email already used',
                ],
                JsonResponse::HTTP_BAD_REQUEST
            );
        }

        return new JsonResponse(
            [
                'message' => 'User successfully created'
            ],
            JsonResponse::HTTP_CREATED
        );
    }

    /**
     * Registration Confirmation view
     *
     * @param string $confirmationToken
     *
     * @return Response
     *
     * @Route("register/confirm/{token}", name="app_security_registerconfirm", methods={"GET"})
     */
    public function registerConfirm(string $token): Response
    {
        $user = $this->em->getRepository(User::class)->getByConfirmationToken($token);

        if (!$user) {
            throw new NotFoundHttpException();
        }

        $user->setConfirmationToken(null);
        $user->setEnabled(true);
        $this->flushDatas($user);

        return $this->render('security/registerConfirm.html.twig');
    }

    /**
     * Register
     *
     * @param Request $request
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @param SwiftMailerService $mailer
     *
     * @return RedirectResponse|Response
     *
     * @Route("/register", name="app_security_register", methods={"POST"})
     */
    public function register(
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        SwiftMailerService $mailer
    ) {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // 5) Log information and send registration email
            $this->logger->info('A new Person has registered');
            $mailer->registerSuccess($user);


            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('app_security_registersuccess');
        }

        return $this->render(
            'security/register.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * User Login
     * 
     * @Route("/login", name="app_security_login")
     *
     * @param AuthenticationUtils $authenticationUtils
     * 
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * Logout
     *
     * @param Request $request
     *
     * @return RedirectResponse
     * 
     * @Route("/logout", name="app_security_logout")
     */
    public function logout(Request $request)
    {
        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();

        return $this->redirectToRoute('app_security_login');
    }
}
