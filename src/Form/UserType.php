<?php

namespace App\Form;

use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use App\Entity\User;

/**
 * Class UserType
 *
 * @category  App
 * @package   App\Form
 * @author    Marvin Courcier <courciermarvin@gmail.com>
 * @copyright 2019
 */
class UserType extends AbstractType
{
    /**
     * User BuildForm
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     * 
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        if ($options['resetPassword'] === false) {
            $builder->add('email', TextType::class);
        }

        if ($options['api'] === false) {
            $builder->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => ['label' => 'Password'],
                'second_options' => ['label' => 'Repeat Password'],
            ));
        } else {
            $builder->add('plainPassword', TextType::class);
        }
    }

    /**
     * User Configure Options Form
     *
     * @param OptionsResolver $resolver
     * 
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'api' => false,
            'resetPassword' => false,
            'csrf_protection'   => false,
        ]);
    }
}
