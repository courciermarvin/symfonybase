<?php

namespace App\Model;

/**
 * Class User
 *
 * @category  App
 * @package   App\Model
 * @author    Marvin Courcier <courciermarvin@gmail.com>
 * @copyright 2019
 */
class User
{
    /**
     * Generate User Token
     *
     * @return string
     */
    public function generateToken()
    {
        try {
            return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
        } catch (\Exception $e) {
            die();
        }
    }
}