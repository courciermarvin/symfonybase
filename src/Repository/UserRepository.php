<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use App\Entity\User;

/**
 * Class UserRepository
 *
 * @category  App
 * @package   App\Repository
 * @author    Marvin Courcier <courciermarvin@gmail.com>
 * @copyright 2019
 *
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getByEmail(?string $email)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getByConfirmationToken(string $confirmationToken)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.confirmationToken = :confirmationToken')
            ->setParameter('confirmationToken', $confirmationToken)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
