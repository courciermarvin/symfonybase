<?php

namespace App\Service;

use Psr\Log\LoggerInterface;
use App\Entity\User;
use Twig\Error;

/**
 * Class SwiftMailerService
 *
 * @category  App
 * @package   App\Service
 * @author    Marvin Courcier <courciermarvin@gmail.com>
 * @copyright 2019
 */
final class SwiftMailerService
{
    const USER_REGISTER = 'New User';

    const USER_RESET_PASSWORD = 'Reset Password';

    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    /**
     * @var \Twig_Environment
     */
    private $template;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SwiftMailerService constructor.
     *
     * @param \Swift_Mailer $mailer
     * @param \Twig_Environment $template
     * @param LoggerInterface $logger
     */
    public function __construct(
        \Swift_Mailer $mailer,
        \Twig_Environment $template,
        LoggerInterface $logger
    ) {
        $this->mailer = $mailer;
        $this->template = $template;
        $this->logger = $logger;
    }

    /**
     * Send Registration Confirmation Email
     *
     * @param User $user
     *
     * return void
     */
    public function registrationConfirmation(User $user): void
    {
        $infoData = [
            'userid' => $user->getId(),
            'email' => $user->getEmail(),
            'event' => 'register',
        ];

        try {
            $message = (new \Swift_Message('Confirmation d\'inscription'))
                ->setFrom('noreply.crossfitchelles@gmail.com')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->template->render(
                        'email/registrationConfirmation.html.twig',
                        [
                            'email' => $user->getEmail(),
                            'token' => $user->getConfirmationToken()
                        ]
                    ),
                    'text/html'
                );

            $this->mailer->send($message);
            $this->logger->info('Register', $infoData);
        } catch (Error\LoaderError $loaderError) {
            $this->logger->error(self::USER_REGISTER, $infoData);
        } catch (Error\RuntimeError $runtimeError) {
            $this->logger->error(self::USER_REGISTER, $infoData);
        } catch (Error\SyntaxError $syntaxError) {
            $this->logger->error(self::USER_REGISTER, $infoData);
        }
    }

    /**
     * Reset User Password Email
     *
     * @param User $user
     *
     * @return void
     */
    public function resetPassword(User $user): void
    {
        $infoData = [
            'userid' => $user->getId(),
            'email' => $user->getEmail(),
            'event' => 'resetPassword',
        ];

        try {
            $message = (new \Swift_Message('Re-initialisation de mot-de-passe'))
                ->setFrom('noreply.crossfitchelles@gmail.com')
                ->setTo($user->getEmail())
                ->setBody(
                    $this->template->render(
                        'email/resetPassword.html.twig',
                        [
                            'email' => $user->getEmail(),
                            'token' => $user->getConfirmationToken()
                        ]
                    ),
                    'text/html'
                );

            $this->mailer->send($message);
            $this->logger->info('Register', $infoData);
        } catch (Error\LoaderError $loaderError) {
            $this->logger->error(self::USER_REGISTER, $infoData);
        } catch (Error\RuntimeError $runtimeError) {
            $this->logger->error(self::USER_REGISTER, $infoData);
        } catch (Error\SyntaxError $syntaxError) {
            $this->logger->error(self::USER_REGISTER, $infoData);
        }
    }
}
