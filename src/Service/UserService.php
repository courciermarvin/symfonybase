<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserService
 *
 * @category  App
 * @package   App\Service
 * @author    Marvin Courcier <courciermarvin@gmail.com>
 * @copyright 2019
 */
class UserService
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    private $repository;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $userPasswordEncoder;

    /**
     * @var SwiftMailerService
     */
    private $mailerService;

    /**
     * UserService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $userPasswordEncoder
     * @param SwiftMailerService $mailerService
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserPasswordEncoderInterface $userPasswordEncoder,
        SwiftMailerService $mailerService
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(User::class);
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->mailerService = $mailerService;
    }

    /**
     * Find User By Email
     *
     * @param string $email
     *
     * @return User|null
     */
    public function findByEmail(string $email): ?User
    {
        return $this->repository->getByEmail($email);
    }

    /**
     * Generate Token
     *
     * @return string
     */
    public function generateToken()
    {
        try {
            return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
        } catch (\Exception $e) {
            die();
        }
    }

    /**
     * Register User
     *
     * @param User $user
     */
    public function registerUser(User $user)
    {
        $user->setEmail($user->getEmail());
        $password = $this->userPasswordEncoder->encodePassword($user, $user->getPlainPassword());
        $user->setPassword($password);
        $user->setConfirmationToken($this->generateToken());
        $user->eraseCredentials();
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        $this->mailerService->registrationConfirmation($user);
    }
}